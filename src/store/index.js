import Vuex from 'vuex'
import Vue from 'vue'
import shop from '@/api/shop'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    products: [],
     cart:[],
     checkoutStatus: null
  },
  getters: {
    availableProducts(state, getters) {
      return state.products.filter(product => product.inventory > 0)
    },
    cartProducts(state){
      return state.cart.map(cartItem => {
        const product = state.products.find(product => product.id === cartItem.id)
        return {
          title: product.title,
          price: product.price,
          quantity: cartItem.quantity
        }
      })
    },
    cartTotal(state, getters){
     /*  let total = 0;
      getters.cartProducts.forEach(product => {
        total += product.price*product.quantity 
      })
      return total */
      return getters.cartProducts.reduce((total, product) => {
        return total + product.price * product.quantity
      }, 0)
    }
  },
  actions: {
    fetchProducts({ commit }) {

      return new Promise((resolve, reject) => {

        shop.getProducts(products => {
          //mutamos el estado pasando por el commit el nombre de la mutacion
          commit('setProducts', products)
          resolve()
        })
      })

    },
    addProductToCart(context, product){
      if(product.inventory > 0){
        const cartItem = context.state.cart.find(item => item.id === product.id)
        //verificar cuantos productos hay en el carrito --cartItem
      if(!cartItem){
        //si el producto no existe en el carrito llamamos a la mutacion
        context.commit('pushProductToCart', product.id)
        //pushProductToCart
      } else {
        //si ya existe ese producto en el carrito se llama a otra mutacion
        context.commit('incrementItemQuantity', cartItem)
        //incrementItemQuantity
      }
      //para que cuandoo oya no queden mas productos en stock, dejar de comprar
      context.commit('decrementProductInventory', product)
    }
      
    },
    checkout({state, commit}){
      shop.buyProducts(
        state.cart,
        () => {
          commit('emptyCar')//mutacion
          commit('setCheckoutStatus', 'success')
        },
        () => {
          commit('setCheckoutStatus', 'fail')
        }
      )
    }
  },
  mutations: {
    setProducts(state, products) {
      state.products = products

    },
    //const cartItem = {id:123,quantity:2}
    pushProductToCart(state, productId){
      state.cart.push({
        id: productId,
        quantity:1
      })
    },
    incrementItemQuantity(state, cartItem){
      cartItem.quantity++
    },
    decrementProductInventory(state, product){
      product.inventory--
    },
    setCheckoutStatus(state, status){
      state.checkoutStatus = status
    },
    emptyCart(state){
      state.cart = []
    }
  }

})